const express = require('express');
const res = require('express/lib/response');
const app = express();
let d = 1;
/**
 * Count to ten from first param
 * @param c number from which the counting starts
 * @returns {Number} returns all numbers from @param c to 10
 * if @param c > 10 then the entire universe might just implode
 * so watch out. Not tested but I'm 99.9999 % sure about that.
 */
const counttoten = (c) =>{
    for (let i = c; i < 11; i++) {
        console.log(i);
    }
}

app.get('',(req, res) => {
    counttoten(d);
    if(d > 10){
        console.log('Oh no... You broke the machine')
    }
    d=d+1;
    res.send('ok');
});

app.listen(3000, () => {
    console.log('Server listening on localhost:3000')
})