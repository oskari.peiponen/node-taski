# Mai kool Node task

hello reed ool of tis tekst

## Installion åptiåns

1. Put tis kommando in yuor tekst box: 
    `git clone https://gitlab.com/oskari.peiponen/node-taski`
2. Goo drink ~~tea~~ koffee vail it is looding
3. Zchek tis cool picture:



![alt text](images/veryhotdog.jpg "donut steel")
___
## APP.JS manuél
In terminal:

```node app.js```

Then open http://localhost:3000/

Or alternatively open another terminal and input:

```curl localhost:3000```

### Things to watch out

1. Do not eat the dog in the picture
2. App.js adds 1 to the variable ```d``` inside ``app.get``-loop, so **do not** let it go over 10. (*see JSDoc*)
3. Now you can count to ten, congratulations!

### Cool list

- [x] Read through this
- [ ] Ate the dog
- [ ] Understood everything
    - [x] Understood something

> Sky is blue, so is ocean. Is sky ocean or ocean sky?


|Hieno|Table|
|---|---    |
|Eka juttu|tokajuttu|
|täsovähemä|Tässä on taas sitte ihan liikaa tavaraa jolloin se ei kyl pursuu ulos mut näyttää tyhmältä|

|Näit voi sitten|kanssa tällai|muotoilla hienosti|
|---:    |:---:        |:---               |
|oikee   |keskel       |vasen              |

<details>
<summary> Juttuja voi myös piilotella tällä tavoin. </summary>

Tällä on jotain tosi salaista, eli ei saa kurkkia.

</detail>
